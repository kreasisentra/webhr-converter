package org.ks.base

class AlertTagLib {
	static namespace = "ks"
	
	/**
	 * Output command object errors in an closable alert-box
	 * require jquery and bootstrap
	 *
	 * @attr command	The command or domain object object
	 */
	def commandErrorMsg = { attrs, body -> 
		def command = attrs.command
		
		if (!command) 
			throw new IllegalStateException("Property [command] must be set!")
		
		out << render(template:"/common/alert/commandErrorTemplate", model:[object:command])
	}
	
	def successMsg = { attrs, body ->
		out << render(template:"/common/alert/successMsgTemplate", model:[object:attrs.flashmsg])
	}
		
	def errorMsg = { attrs, body ->
		out << render(template:"/common/alert/errorMsgTemplate", model:[object:attrs.flashmsg])
	}
	
	//static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
}
