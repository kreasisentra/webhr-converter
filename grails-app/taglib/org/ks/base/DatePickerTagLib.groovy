package org.ks.base

class DatePickerTagLib {
	static namespace = "ks"
	
	def datePicker = { attrs, body ->
		out << render(template:"/common/other/datepickerTemplate", model:[id:attrs.id,nilai:attrs.nilai,name:attrs.name,tglFormat:attrs.tglFormat])
	}
	
	//static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
}
