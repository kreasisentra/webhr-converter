<g:set var="dateFormat" value="${tglFormat}"/>
<g:set var="dateFormatJS" value="yy-mm-dd"/>
<g:if test="${nilai != ''}"  >
	<g:if test="${nilai instanceof Date}"  >
		<g:textField name="${name}" class="form-control" id="${id}" value="${formatDate(format: dateFormat, date: nilai)}" />
	</g:if>
	<g:else>
		<g:textField name="${name}" class="form-control" id="${id}" value="${nilai}" />
	</g:else>
</g:if>
<g:else >
	<g:textField name="${name}" class="form-control" id="${id}" value="" />
</g:else>


<asset:script type="text/javascript">
		$("#"+"${id}").datepicker({
			dateFormat: "${dateFormatJS}"
		});

</asset:script>