<g:if test="${object}" >
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-success fade in">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<i class="glyphicon glyphicon-ok-sign"></i> ${object}
		</div>
	</div>
</div>
</g:if>
