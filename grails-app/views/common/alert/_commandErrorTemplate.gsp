<g:if test="${object.hasErrors()}">
<div class='alert alert-danger fade in'>
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<i class="fa fa-times-circle fa-fw fa-lg"></i>
	<strong>Error(s): </strong>
	<ul>
	<g:eachError bean="${object.errors}" >
		<li><g:message error="${it}"/></li>
	</g:eachError>
	</ul>
</div>
</g:if>