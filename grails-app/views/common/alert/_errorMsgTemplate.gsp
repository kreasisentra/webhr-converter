<g:if test="${object}" >
<div class="row">
	<div class="col-lg-12">
		<div class="alert alert-danger fade in">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<i class="fa fa-times-circle fa-fw fa-lg"></i> ${object ?: null}
		</div>
	</div>
</div>
</g:if>