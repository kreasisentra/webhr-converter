<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Upload Form</title>
	</head>
	
	<body>
		<br />
		<div class="col-lg-2">
		</div>
		<div class="col-lg-10">
			<div class="row">
					<div class="col-lg-6">
					<ks:errorMsg flashmsg="${flash.error}" />
					</div>
			</div>
			<g:form action="uploadData" enctype="multipart/form-data" useToken="true">
				<div class="row">
					<div class="col-lg-10">
						<div class="form-group">
						<input type="file" name="myFile" class="form-control" />
						</div>
					</div>
				</div>
				
				<div class="row">
						<div class="col-lg-6">
							<div class="form-group" style="text-align: center;">
								<input type="submit" value="${labelBtnConvert}" class="btn btn-primary" />
							</div>
						</div>
				</div>
				<br />
			 </g:form>
		 </div>
	
	<asset:script type="text/javascript">
	</asset:script>
	</body>
</html>
