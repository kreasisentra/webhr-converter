package org.ks.webhrconverter

import grails.transaction.Transactional

@Transactional
class DataService {

    def getUsernameWebhr(String usernameMachine) {
		String output = null
		
		InputStream fileIn = this.class.classLoader.getResourceAsStream('datamapping.csv')
		fileIn.splitEachLine(",") { fields ->
			if (usernameMachine!=null && usernameMachine.equals(fields[0])) {
				output = fields[1]
			}
		}
		
		return output
    }
}
