package org.ks.webhrconverter

import java.text.SimpleDateFormat


class UploadController {
	public static final String FILENAME_OUTPUT = "webhr"
	public static final String FILENAME_TIMESTAMP = "yyyyMMdd_HHmm"
	public static final String[] HEADER_OUTPUT = ["Username", "Date", "Time In", "Time Out"]
	public static final String LABEL_BUTTON_CONVERT = "Convert to WebHR Import csv"
	public static final String CUTOFF_TIMEOUT = "17:00"
	public static final String CUTOFF_TIMEIN = "06:00"
	public static final String END_TIMEOUT = "23:59:59"
	
	def dataService
	
	
    def index() { }
	
	def uploadForm() {
		
		render view: "uploadForm", model: [labelBtnConvert: LABEL_BUTTON_CONVERT]
	}
	
	def uploadData() {
		def output = ""
		
		def fileName
		def f = request.getFile('myFile')
		fileName = f.getOriginalFilename()
		
		if (fileName=='' || fileName==null) {
			flash.error = "File Null"
			render view:"uploadForm"
			return
		}
		
		List<Attendance> detail = new ArrayList<Attendance>()
		def dataFiles = []  
		def tempName = ""
		def tempDate = ""
		int i = 0
		int countAll = 0;
		int countUsername = 0
		int countUserDate = 0
		
		request.getFile('myFile')
		.inputStream
		.splitEachLine('\t') { fields ->
			
			def date = fields[2].substring(0, 10)
			def time = fields[2].substring(11, 19)
			
			if ((tempName.equals(fields[1]) && !tempDate.equals(date)) || 
			     (!tempName.equals(fields[1])) || countUsername==0) { 
				 
				Attendance data = new Attendance()
				data.username = dataService.getUsernameWebhr(fields[1])
				
				if (data.username != null) {
					countUserDate = 0
					
					data.date = date
					
					if (time.compareTo(CUTOFF_TIMEOUT)>=0) {
						data.timeIn = ""
						data.timeOut = time
					} else {
						data.timeIn = time
					}
					
					if (time.compareTo(CUTOFF_TIMEIN)<0) {
						data = detail.get(i-1)
						data.timeIn = ""
						data.timeOut = END_TIMEOUT
						
						data.date = tempDate
						date = data.date
						time = data.timeOut
						
						countUserDate++
						
						data.countUserDate = countUserDate
						data.countAll = countAll
						
					} else {
						countUserDate++
						
						detail.push(data)
						
						i++
						countUsername++
					}
				}
				
			} else {
				Attendance data = detail.get(i-1)
				
				countUserDate++
				
				data.countUserDate = countUserDate
				data.countAll = countAll
			}
			
			def dataFile = [:]
			dataFile.username = fields[1]
			dataFile.date = date
			dataFile.time = time
			dataFiles << dataFile
			
			tempName = fields[1]
			tempDate = date
			
			countAll++
		}
		
		// header
		i=0
		HEADER_OUTPUT.each {
			if (i==0) {
				output = it
			} else {
				output += "," + it
			}
			
			i++ 
		}
		output += String.format('%n') //carriage return
		
		i=0
		detail.each {
			// Time Out set with last line
			if (it.countAll!=null) {	
				def dataFile = [:]
				dataFile = dataFiles[it.countAll]
				
				it.timeOut = dataFile.time
			}
			
			output += it.username + ","
			output += it.date + ","
			output += it.timeIn + ","
			output += it.timeOut==null? "" :it.timeOut
			output += String.format('%n')
			i++
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(FILENAME_TIMESTAMP)
		String csvOutput = FILENAME_OUTPUT + "_" + sdf.format(new Date()) + ".csv"
		
		response.setContentType( "application-xdownload")
		response.setHeader("Content-Disposition", "attachment;filename=" + csvOutput)
		response.getOutputStream() << output
		
		flash.message = 'Success'
		
		render view: "uploadData", model: [output: output]
	}
	
}
