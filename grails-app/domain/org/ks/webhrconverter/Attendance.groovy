package org.ks.webhrconverter

class Attendance {
	String username
	String date
	String timeIn 
	String timeOut
	Integer countAll
	Integer countUserDate

	@Override
	public String toString() {
		return "Attendance [username=" + username + ", date=" + date + ", timeIn=" + timeIn + ", timeOut=" + timeOut + ", countAll=" + countAll + ", countUserDate=" + countUserDate + "]";
	}	
	
}
